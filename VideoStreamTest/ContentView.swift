// Copyright 2021 Robo.Fish UG

import SwiftUI
import RFVideoStream
import RFHistogram

struct ContentView: View
{
	@AppStorage("showHistogram")
	var showHistogram : Bool = false

	let stream = RFVideoStream(source:RFCamera(cameraType: .frontWide)!)

	var body: some View
	{
		VStack(spacing: 10)
		{
			if showHistogram
			{
				RFHistogramView(stream: stream)
			}
			else
			{
				RFVideoView(stream: stream)
			}

			Toggle(isOn: $showHistogram) { Text("Histogram") }
				.frame(width:200)
				.padding([.bottom], 20)
		}
	}
}
