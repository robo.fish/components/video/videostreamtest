// Copyright 2021 Robo.Fish UG

import SwiftUI

@main
struct VideoStreamTestApp: App
{
	var body: some Scene
	{
		WindowGroup
		{
			ContentView()
		}
	}
}
